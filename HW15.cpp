#include <iostream>
#include <cmath>

//��������� � �������� ���������� ������� �� ������� �� 2 (1 ��� 0) � ������������ �����.
int HwTask(int remain, int limit)
{   
    std::cout << "Result: ";
    //��� ������ �����:
    if (remain == 0)
    {
        int i = 0;
        while (i <= limit)
        {
            if (i % 2 == 0)
            {
                std::cout << i << " ";
            }
            ++i;
        }
    }
    //��� �������� �����:
    else
    {
        int i = 0;
        while (i <= limit)
        {
            if (i % 2 == 1)
            {
                std::cout << i << " ";
            }
            ++i;
        }
    }
    std::cout << '\n';
    return 0;
}

//�������� ����������������� �������.
int main()
{
    std::cout << HwTask(0, 10)<< '\n';
    std::cout << HwTask(1, 10);
}